const express = require('express');
const cors = require("cors");
const { Pool } = require('pg');

const pool = new Pool({
    connectionString: process.env.URLDB
});

const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

let app = express();

// ===========================
//  Obtener lista de Todo
// ===========================

// 
app.get('/todo', cors(), (request, response) => {
    pool.query('SELECT * FROM todo ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }

        console.log(results);

        response.status(200).json(results.rows);

    });
});

// Consulta un ToDo especifico
app.get('/todo/:id', cors(), (request, response) => {

    const id = parseInt(request.params.id);

    pool.query('SELECT * FROM todo WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }

        console.log(results);

        response.status(200).json(results.rows);

    });
});

// Crea registro de ToDo
app.post('/todo', cors(), (request, response) => {

    const [tarea, responsable, fecha, completada] = request.body;

    pool.query('INSERT INTO todo (tarea, responsable, fecha, completada) VALUES ($1, $2, $3, $4)', [tarea, responsable, fecha, completada], (error, results) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'ToDo creado satisfactoriamente'
        });

    });
});

// Actualización de ToDo
app.put('/todo', cors(), (request, response) => {

    const id = parseInt(request.params.id);
    const [tarea, responsable, fecha, completada] = request.body;

    pool.query('UPDATE todo SET tarea=$2, responsable=$3, fecha=$4, completada=$5) WHERE id=$1', [id, tarea, responsable, fecha, completada], (error, results) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'ToDo actualizado satisfactoriamente'
        });

    });
});

// Eliminación de ToDo
app.delete('/todo', cors(), (request, response) => {

    const id = parseInt(request.params.id);

    pool.query('DELETE FROM todo WHERE id=$1', [id], (error, results) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'ToDo eliminado satisfactoriamente'
        });

    });
});

module.exports = app;
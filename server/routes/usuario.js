const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');
const { Pool } = require('pg');

const { verificaToken } = require('../middlewares/autenticacion');

const pool = new Pool({
    connectionString: process.env.URLDB
});

const app = express();

// ===========================
//  Obtener lista de Usuarios
// ===========================

// 
app.get('/usuario', (request, response) => {
    pool.query('SELECT * FROM usuario ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }

        console.log(results);

        response.status(200).json(results.rows);

    });
});

// Consulta un Usuario especifico
app.get('/usuario/:id', (request, response) => {

    const id = parseInt(request.params.id);

    pool.query('SELECT * FROM usuario WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }

        console.log(results);

        response.status(200).json(results.rows);

    });
});


//app.post('/usuario', [verificaToken, verificaAdmin_Role], function(req, res) {
app.post('/usuario', (req, res) => {

    let body = req.body;

    let encryptPass = bcrypt.hashSync(body.clave, 10);

    pool.query('INSERT INTO public.usuario(nombre, correo, clave) VALUES ($1, $2, $3)', [body.nombre, body.correo, encryptPass], (err, usuarioBD) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'Usuario creado satisfactoriamente'
        });

    });

});

// Actualización de Usuario
app.put('/usuario', (request, response) => {

    const id = parseInt(request.params.id);
    const [nombre, correo] = request.body;

    pool.query('UPDATE usuario SET nombre=$2, correo=$3) WHERE id=$1', [id, nombre, correo], (error, results) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'ToDo actualizado satisfactoriamente'
        });

    });
});

// Elimina Usuario
app.delete('/usuario', (request, response) => {

    const id = parseInt(request.params.id);

    pool.query('DELETE FROM usuario WHERE id=$1', [id], (error, results) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'Usuario eliminado satisfactoriamente'
        });

    });
});

module.exports = app;
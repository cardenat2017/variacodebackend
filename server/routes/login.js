const express = require('express');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Pool } = require('pg');

const app = express();

const pool = new Pool({
    connectionString: process.env.URLDB
});

app.post('/login', (req, res) => {

    let body = req.body;

    pool.query("SELECT clave, nombre FROM usuario WHERE correo = $1::text", [body.correo], (error, usuarioBD) => {

        if (error) {
            return res.status(500).json({
                ok: false,
                err: error
            });
        }

        if (!usuarioBD) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: '(Usuario) o contraseña incorrectos'
                }
            });
        }

        let encryptPass = bcrypt.hashSync(body.clave, 10);

        if (!bcrypt.compareSync(body.clave, usuarioBD.rows[0].clave)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario o (contraseña) incorrectos'
                }
            });
        }

        let token = jwt.sign({
            usuario: usuarioBD.rows
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

        res.json({
            ok: true,
            usuario: usuarioBD.rows[0],
            token: token
        });
    });

});

module.exports = app;
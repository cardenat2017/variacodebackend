const express = require('express');

const { Pool } = require('pg');

const pool = new Pool({
    connectionString: process.env.URLDB
});

const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

let app = express();

// ===========================
//  Obtener productos
// ===========================

app.get('/productos', (request, response) => {
    pool.query('SELECT * FROM producto ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }

        console.log(results);

        response.status(200).json(results.rows);

    });
});

module.exports = app;
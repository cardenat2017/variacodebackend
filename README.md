# Proyecto Backend de Prueba para Variacode

## Desarrollado con NodeJS

## Requisitos de instalación:
	NPM versión 6.4.1
	NODE versión 10.15.3

## Observaciones:
	La aplicación se conecta a una base de datos PostgreSQL en la nube https://elephantsql.com

## Pasos para activar la funcionalidad del Aplicativo
	1. Clonar el repositorio localmente con el siguiente comando: 
	   `git clone https://cardenat2017@bitbucket.org/cardenat2017/variacodebackend.git`
	2. Dentro del directorio variacodebackend ejecutar: 
	   `npm install`
	3. Posteriormente ejecutar el comando: 
	   `nodemon server/server`
	4. Automáticamente activará la aplicación para escuchar peticiones en el puerto 4000

## Bugs conocidos
	Ninguno
